﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using brainfucker.Ast;

namespace brainfucker.Tests
{
	[TestClass]
	public class ParserTests
	{
		[TestMethod]
		public void Parser_WhenGivenNoTokens_ReturnsEmptyProgram()
		{
			List<Token> tokens = new List<Token> { };

			Parser parser = new Parser();
			Ast.Program program = parser.Parse(tokens.GetEnumerator());

			Assert.IsFalse(program.SubOperations.Any());
		}

		[TestMethod]
		public void Parser_WhenGivenOnlyEOF_ReturnsEmptyProgram()
		{
			List<Token> tokens = new List<Token> { Token.EOF };

			Parser parser = new Parser();
			Ast.Program program = parser.Parse(tokens.GetEnumerator());

			Assert.IsFalse(program.SubOperations.Any());
		}

		[TestMethod]
		public void Parser_WhenGivenASingleToken_ReturnsProgramWithSingleOperation()
		{
			List<Token> tokens = new List<Token> { Token.PERIOD };

			Parser parser = new Parser();
			Ast.Program program = parser.Parse(tokens.GetEnumerator());

			Assert.AreEqual(1, program.SubOperations.Count());
		}

		[TestMethod]
		public void Parser_WhenGivenSimpleTokens_ParsesCorrectly()
		{
			List<Token> tokens = new List<Token> { Token.COMMA, Token.GT, Token.LT, Token.MINUS, Token.PERIOD, Token.PLUS };

			Parser parser = new Parser();
			Ast.Program program = parser.Parse(tokens.GetEnumerator());

			List<Operation> operations = program.SubOperations.ToList();

			//Assert
			Assert.AreEqual(6, program.SubOperations.Count());
			Assert.IsTrue(operations[0] is ReadData);
			Assert.IsTrue(operations[1] is IncrementDataPointer);
			Assert.IsTrue(operations[2] is DecrementDataPointer);
			Assert.IsTrue(operations[3] is DecrementData);
			Assert.IsTrue(operations[4] is WriteData);
			Assert.IsTrue(operations[5] is IncrementData);
		}

		[TestMethod]
		public void Parser_WhenGivenLoopTokens_ParsesCorrectly()
		{
			List<Token> tokens = new List<Token> { Token.COMMA, Token.GT, Token.LT, Token.LBRACKET, Token.MINUS, Token.PERIOD, Token.RBRACKET, Token.PLUS };

			Parser parser = new Parser();
			Ast.Program program = parser.Parse(tokens.GetEnumerator());

			List<Operation> operations = program.SubOperations.ToList();

			//Assert
			Assert.AreEqual(5, program.SubOperations.Count());
			Assert.IsTrue(operations[0] is ReadData);
			Assert.IsTrue(operations[1] is IncrementDataPointer);
			Assert.IsTrue(operations[2] is DecrementDataPointer);
			Assert.IsTrue(operations[3] is LoopOperation);
			Assert.IsTrue(operations[4] is IncrementData);

			LoopOperation loop = operations[3] as LoopOperation;

			List<Operation> loopOperations = loop.SubOperations.ToList();

			Assert.AreEqual(2, loopOperations.Count);
			Assert.IsTrue(loopOperations[0] is DecrementData);
			Assert.IsTrue(loopOperations[1] is WriteData);
		}

		[TestMethod]
		[ExpectedException(typeof(ParseException), "Unbalanced started loop did not result in the expected parsing error")]
		public void Parser_WhenGivenUnbalancedStartedLoop_ReportsError()
		{
			List<Token> tokens = new List<Token> { Token.COMMA, Token.GT, Token.LT, Token.LBRACKET, Token.MINUS, Token.PERIOD, Token.PLUS };

			Parser parser = new Parser();
			Ast.Program program = parser.Parse(tokens.GetEnumerator());
		}

		[TestMethod]
		[ExpectedException(typeof(ParseException), "Unbalanced ended loop did not result in the expected parsing error")]
		public void Parser_WhenGivenUnbalancedEndingLoop_ReportsError()
		{
			List<Token> tokens = new List<Token> { Token.COMMA, Token.GT, Token.LT, Token.RBRACKET, Token.MINUS, Token.PERIOD, Token.PLUS };

			Parser parser = new Parser();
			Ast.Program program = parser.Parse(tokens.GetEnumerator());
		}
	}
}
