﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using brainfucker.Ast;

namespace brainfucker.Tests
{
	[TestClass]
	public class CodeGeneratorTests
	{
		[TestMethod]
		public void CodeGenerator_WhenGivenEmptyProgram_GeneratesFile()
		{
			//Arrange
			CodeGenerator gen = new CodeGenerator();
			var program = new Ast.Program(Enumerable.Empty<Ast.Operation>());

			//Act
			var asm = gen.Generate(program, "bfmodule.exe");

			//Assert
			Type programType = asm.GetType("Program");
			Assert.IsNotNull(programType.GetMethod("Main"));
		}

		[TestMethod]
		public void CodeGenerator_WhenGivenProgramIncrementingData_GeneratedProgramDoesNotThrow()
		{
			List<Operation> operations = new List<Operation>() { new IncrementData() };

			//Arrange
			CodeGenerator gen = new CodeGenerator();
			var program = new Ast.Program(operations);

			//Act
			var asm = gen.Generate(program, "bfmodule.exe");

			Type generatedType = asm.GetType("Program");
			MethodInfo mainMethod = generatedType.GetMethod("Main");
			mainMethod.Invoke(null, new object[] { });

			Assert.IsTrue(true);
		}

		[TestMethod]
		public void CodeGenerator_WhenGivenProgramIncrementingData65Times_GeneratesProgramWritingA()
		{
			///Arrange
			///

			List<Operation> operations = GetIncrementOperationsForChar('A');

			operations.Add(new WriteData());

			//Redirect std output to a StringBuilder that we can inspect
			var sb = new StringBuilder();
			Console.SetOut(new System.IO.StringWriter(sb));

			CodeGenerator gen = new CodeGenerator();
			var program = new Ast.Program(operations);

			///Act
			///

			//Generate program
			var asm = gen.Generate(program, "bfmodule.exe");

			//Run program
			Type generatedType = asm.GetType("Program");
			MethodInfo mainMethod = generatedType.GetMethod("Main");
			mainMethod.Invoke(null, new object[] { });

			///Assert
			Assert.AreEqual("A", sb.ToString());
		}

		[TestMethod]
		public void CodeGenerator_WhenReadingA_GeneratesProgramWritingA()
		{
			///Arrange
			///

			List<Operation> operations = new List<Operation>();

			operations.Add(new ReadData());
			operations.Add(new WriteData());

			Console.SetIn(new System.IO.StringReader("A"));

			//Redirect std output to a StringBuilder that we can inspect
			var sb = new StringBuilder();
			Console.SetOut(new System.IO.StringWriter(sb));

			CodeGenerator gen = new CodeGenerator();
			var program = new Ast.Program(operations);

			///Act
			///

			//Generate program
			var asm = gen.Generate(program, "bfmodule.exe");

			//Run program
			Type generatedType = asm.GetType("Program");
			MethodInfo mainMethod = generatedType.GetMethod("Main");
			mainMethod.Invoke(null, new object[] { });

			///Assert
			Assert.AreEqual("A", sb.ToString());
		}

		/// <summary>
		/// Uses the IncrementData, IncrementDataPointer and WriteData operations to write the string "ABC"
		/// </summary>
		[TestMethod]
		public void CodeGenerator__CorrectlyIncrementsAndDecrementsDataPointer()
		{
			///Arrange
			///

			List<Operation> programOperations = new List<Operation>();

			programOperations.AddRange(GetIncrementOperationsForChar('A')); //Increment first data cell to 'A'
			programOperations.Add(new IncrementDataPointer()); //Move to second cell
			programOperations.AddRange(GetIncrementOperationsForChar('B')); //Increment second data cell to 'B'
			programOperations.Add(new IncrementDataPointer()); //Move to third cell
			programOperations.AddRange(GetIncrementOperationsForChar('C')); //Increment third data cell to 'C'
			programOperations.Add(new DecrementDataPointer()); //Move to second cell
			programOperations.Add(new DecrementDataPointer()); //Move to first cell
			programOperations.Add(new WriteData()); //Write contents of first data cell to std out
			programOperations.Add(new IncrementDataPointer()); //Move to second cell
			programOperations.Add(new WriteData()); //Write contents of second data cell to std out
			programOperations.Add(new IncrementDataPointer()); //Move to third cell
			programOperations.Add(new WriteData()); //Write contents of third data cell to std out

			//Redirect std output to a StringBuilder that we can inspect
			var sb = new StringBuilder();
			Console.SetOut(new System.IO.StringWriter(sb));

			CodeGenerator gen = new CodeGenerator();
			var program = new Ast.Program(programOperations);

			///Act
			///

			//Generate program
			var asm = gen.Generate(program, "bfmodule.exe");
			
			//Run program
			Type generatedType = asm.GetType("Program");
			MethodInfo mainMethod = generatedType.GetMethod("Main");
			mainMethod.Invoke(null, new object[] { });

			///Assert
			Assert.AreEqual("ABC", sb.ToString());
		}

		private List<Operation> GetIncrementOperationsForChar(char c)
		{
			List<Operation> operations = new List<Operation>();
			for (int i = 0; i < (int)c; i++)
			{
				operations.Add(new IncrementData());
			}
			return operations;
		}

		/// <summary>
		/// Uses the IncrementData, IncrementDataPointer and WriteData operations to write the string "ABC" to data[1], data[2] and data[3],
		/// uses loop to return data pointer to index 0, moves forward using loop and writes "D", then uses loop to write all data to std out
		/// </summary>
		[TestMethod]
		public void CodeGenerator_WhenUsingLoopToSkipForwardAndRewind_CorrectlyWritesABCD()
		{
			///Arrange
			///

			LoopOperation rewind = new LoopOperation(new List<Operation> { new DecrementDataPointer() });
			LoopOperation skipforward = new LoopOperation(new List<Operation> { new IncrementDataPointer() });
			LoopOperation outputData = new LoopOperation(new List<Operation> { new WriteData(), new IncrementDataPointer() });

			List<Operation> programOperations = new List<Operation>();

			programOperations.Add(new IncrementDataPointer()); //Skip first cell so that we have a 0 cell to stop loops
			programOperations.AddRange(GetIncrementOperationsForChar('A')); //Increment second data cell to 'A'
			programOperations.Add(new IncrementDataPointer()); //Move to third cell
			programOperations.AddRange(GetIncrementOperationsForChar('B')); //Increment third data cell to 'B'
			programOperations.Add(new IncrementDataPointer()); //Move to fourth cell
			programOperations.AddRange(GetIncrementOperationsForChar('C')); //Increment fourth data cell to 'C'

			programOperations.Add(rewind); //Rewind to the beginning
			programOperations.Add(new IncrementDataPointer()); //Increment to point to the 'A' cell
			programOperations.Add(skipforward); //Skip forward to first zero-cell (fifth cell)

			programOperations.AddRange(GetIncrementOperationsForChar('D'));

			programOperations.Add(rewind); //Rewind to very first cell
			programOperations.Add(new IncrementDataPointer()); //Increment to point to the 'A' cell

			programOperations.Add(outputData); //Write all data to output

			//Redirect std output to a StringBuilder that we can inspect
			var sb = new StringBuilder();
			Console.SetOut(new System.IO.StringWriter(sb));

			CodeGenerator gen = new CodeGenerator();
			var program = new Ast.Program(programOperations);

			///Act
			///

			//Generate program
			var asm = gen.Generate(program, "bfmodule.exe");
			asm.Save("bfmodule.exe");
			//Run program
			Type generatedType = asm.GetType("Program");
			MethodInfo mainMethod = generatedType.GetMethod("Main");
			mainMethod.Invoke(null, new object[] { });

			///Assert
			Assert.AreEqual("ABCD", sb.ToString());
		}
	}
}
