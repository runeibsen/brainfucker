﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace brainfucker.Tests
{
	[TestClass]
	public class LexerTests
	{
	[TestMethod]
	public void Lexer_WhenGivenStringContainingAllCharacters_LexesCorrectly()
	{
		///Arrange
		TextReader tr = new StringReader("><+-.,[]");
		Lexer lexer = new Lexer(tr);
		List<Token> tokens = new List<Token>();

		///Act
		while (lexer.MoveNext())
		{
			tokens.Add(lexer.Current);
		}

		///Assert
		Assert.AreEqual(9, tokens.Count);
		Assert.AreEqual(Token.GT,		tokens[0]);
		Assert.AreEqual(Token.LT,		tokens[1]);
		Assert.AreEqual(Token.PLUS,		tokens[2]);
		Assert.AreEqual(Token.MINUS,	tokens[3]);
		Assert.AreEqual(Token.PERIOD,	tokens[4]);
		Assert.AreEqual(Token.COMMA,	tokens[5]);
		Assert.AreEqual(Token.LBRACKET,	tokens[6]);
		Assert.AreEqual(Token.RBRACKET,	tokens[7]);
		Assert.AreEqual(Token.EOF,		tokens[8]);
	}
	}
}
