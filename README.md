# brainfucker#

brainfucker is a [brainfuck](http://en.wikipedia.org/wiki/Brainfuck) compiler for .NET.

### Getting started ###

* Download the source code
* Compile the Visual Studio solution
* Run `brainfucker.exe`

### Sample session ###

To compile and run the infamous Hello World program using **brainfucker**, you run `brainfucker.exe` with either inline brainfuck source code or the path to your source code as an argument. `brainfucker.exe`  will generate a .NET executable called `bfmodule.exe`. In the following sample, `brainfucker.exe` is invoked with the `/t` switch, which indicates that the source code is being passed as inline text:

```
prompt> brainfucker.exe /t:"++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++."

prompt> bfmodule.exe
Hello World!

prompt>
```

Brainfuck programs tend to be rather long and unwieldy, so it may be desirable to read them from a file. You do this using the `/f` switch (assuming your Brainfuck source code is stored in a file called `helloworld.bf`:

```
prompt> brainfucker.exe /f:helloworld.bf

prompt> bfmodule.exe
Hello World!

prompt>
```

### Contribution guidelines ###

Feel free to contribute, fork, steal etc. We absolutely accept pull requests!