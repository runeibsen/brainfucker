﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection.Emit;
using System.Reflection;
using brainfucker.Ast;

namespace brainfucker
{
	public class CodeGenerator
	{

		private class SymbolTable 
		{
			public LocalBuilder Data { get;set; }
			public LocalBuilder DataPtr { get; set; }
			public LocalBuilder InstrunctionPtr { get; set; }
		}

		public AssemblyBuilder Generate(Ast.Program program, string moduleName)
		{
			AssemblyBuilder asm = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName(moduleName), AssemblyBuilderAccess.RunAndSave);

			ModuleBuilder mod = asm.DefineDynamicModule(moduleName, moduleName, true);
			TypeBuilder type = mod.DefineType("Program", TypeAttributes.Class);

			MethodBuilder main = type.DefineMethod("Main", MethodAttributes.Public | MethodAttributes.Static);
			ILGenerator il = main.GetILGenerator();

			CodeGenerationVisitor generator = new CodeGenerationVisitor(il);
			generator.Visit(program);

			type.CreateType();
			asm.SetEntryPoint(main);
			return asm;
		}

		private class CodeGenerationVisitor : IVisitor
		{
			private readonly ILGenerator _il;
			private SymbolTable _symbolTable;

			public CodeGenerationVisitor(ILGenerator il)
			{
				_il = il;
			}

			#region IVisitor members
			
			public void Visit(IncrementDataPointer operation)
			{
				//Put datapointer's value onto the stack
				_il.Emit(OpCodes.Ldloc, _symbolTable.DataPtr.LocalIndex);

				//Add 1
				_il.Emit(OpCodes.Ldc_I4_1);
				_il.Emit(OpCodes.Add);

				//Save datapointer's value
				_il.Emit(OpCodes.Stloc, _symbolTable.DataPtr.LocalIndex);
			}

			public void Visit(DecrementDataPointer operation)
			{
				//Put datapointer's value onto the stack
				_il.Emit(OpCodes.Ldloc, _symbolTable.DataPtr.LocalIndex);

				//Subtract 1
				_il.Emit(OpCodes.Ldc_I4_1);
				_il.Emit(OpCodes.Sub);

				//Save datapointer's value
				_il.Emit(OpCodes.Stloc, _symbolTable.DataPtr.LocalIndex);
			}

			public void Visit(IncrementData operation)
			{
				//Put the reference to the data array onto the stack
				_il.Emit(OpCodes.Ldloc, _symbolTable.Data.LocalIndex);
				//Put datapointer's value onto the stack
				_il.Emit(OpCodes.Ldloc, _symbolTable.DataPtr.LocalIndex);
				
				//Load current value
				PutDataOntoStack();
				//Add 1
				_il.Emit(OpCodes.Ldc_I4_1);
				_il.Emit(OpCodes.Add);
				
				//The stack now contains a reference to the data array, the datapointer's value and the incremented data value
				//Thus, we can store the incremented value by doing a stelem.i1 which takes (reference to array, index, value) as parameters
				_il.Emit(OpCodes.Stelem_I1);
			}

			public void Visit(DecrementData operation)
			{
				//Put the reference to the data array onto the stack
				_il.Emit(OpCodes.Ldloc, _symbolTable.Data.LocalIndex);
				//Put datapointer's value onto the stack
				_il.Emit(OpCodes.Ldloc, _symbolTable.DataPtr.LocalIndex);

				//Load current value
				PutDataOntoStack();
				//Subtract 1
				_il.Emit(OpCodes.Ldc_I4_1);
				_il.Emit(OpCodes.Sub);

				//The stack now contains a reference to the data array, the datapointer's value and the incremented data value
				//Thus, we can store the incremented value by doing a stelem.i1 which takes (reference to array, index, value) as parameters
				_il.Emit(OpCodes.Stelem_I1);
			}

			public void Visit(WriteData operation)
			{
				PutDataOntoStack();
				//Write data to the console
				_il.Emit(OpCodes.Call, typeof(Console).GetMethod("Write", new Type[] { typeof(char) }));
			}

			public void Visit(ReadData operation)
			{
				//Put the reference to the data array onto the stack
				_il.Emit(OpCodes.Ldloc, _symbolTable.Data.LocalIndex);
				//Put datapointer's value onto the stack
				_il.Emit(OpCodes.Ldloc, _symbolTable.DataPtr.LocalIndex);
				//Read int from input
				_il.Emit(OpCodes.Call, typeof(Console).GetMethod("Read", new Type[] { }));
				_il.Emit(OpCodes.Conv_U1); //Cast to byte
				//Store
				_il.Emit(OpCodes.Stelem_I1);
			}

			public void Visit(Ast.Program operation)
			{
				GenerateSymbolTable(_il);
				
				foreach (var op in operation.SubOperations)
					op.Accept(this);

				_il.Emit(OpCodes.Ret);
			}

			public void Visit(LoopOperation loop)
			{
				Label condition = _il.DefineLabel();
				Label end = _il.DefineLabel();

				_il.MarkLabel(condition);

				//Evaluate condition
				PutDataOntoStack(); //Push current data onto stack
				_il.Emit(OpCodes.Ldc_I4_0); //Push zero onto stack
				_il.Emit(OpCodes.Ceq); //Compare current data value to 0, push 1 onto stack if they are equal, push 0 onto stack if they are different

				//Break from loop if data is zero
				_il.Emit(OpCodes.Brtrue, end);

				foreach (var op in loop.SubOperations)
					op.Accept(this);

				_il.Emit(OpCodes.Br, condition);
				_il.MarkLabel(end);
			}

			#endregion

			private void PutDataOntoStack()
			{
				//Put the reference to the data array onto the stack
				_il.Emit(OpCodes.Ldloc, _symbolTable.Data.LocalIndex);
				//Put datapointer's value onto the stack
				_il.Emit(OpCodes.Ldloc, _symbolTable.DataPtr.LocalIndex);
				//Load the value pointed to by the data pointer onto the stack
				_il.Emit(OpCodes.Ldelem_U1);
			}

			private void GenerateSymbolTable(ILGenerator il)
			{
				///Declare locals
				var dataLocalBuilder = il.DeclareLocal(typeof(byte[]));
				dataLocalBuilder.SetLocalSymInfo("data");

				var dataPtrBuilder = il.DeclareLocal(typeof(Int32));
				dataPtrBuilder.SetLocalSymInfo("dataPtr");

				var instructionPtrBuilder = il.DeclareLocal(typeof(Int32));
				instructionPtrBuilder.SetLocalSymInfo("instructionPtr");

				///Initialize data array
				il.Emit(OpCodes.Ldc_I4, 3000);
				il.Emit(OpCodes.Newarr, typeof(byte));
				il.Emit(OpCodes.Stloc, dataLocalBuilder);

				///Initialize dataPtr
				il.Emit(OpCodes.Ldc_I4, 0);
				il.Emit(OpCodes.Stloc, dataPtrBuilder);

				_symbolTable = new SymbolTable
				{
					Data = dataLocalBuilder,
					DataPtr = dataPtrBuilder,
					InstrunctionPtr = instructionPtrBuilder
				};
			}
		}
	}
}
