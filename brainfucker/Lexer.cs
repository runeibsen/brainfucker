﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace brainfucker
{
public class Lexer : IEnumerator<Token>
{
	private TextReader _reader;
	private Token? _current;

	public Lexer(TextReader reader)
	{
		_reader = reader;
	}

	/// <summary>
	/// Consumes input and identifies and returns the next token
	/// </summary>
	private Token GetToken()
	{
		int ci = _reader.Read();
		if (ci == -1)
		{
			return Token.EOF;
		}

		return ToToken((char)ci);
	}

	private Token ToToken(char c)
	{
		switch(c)
		{
			case '<':
				return Token.LT;
			case '>':
				return Token.GT;
			case '+':
				return Token.PLUS;
			case '-':
				return Token.MINUS;
			case '.':
				return Token.PERIOD;
			case ',':
				return Token.COMMA;
			case '[':
				return Token.LBRACKET;
			case ']':
				return Token.RBRACKET;
			default:
				return Token.UNKNOWN;
		}
	}

	public Token Current
	{
		get 
		{
			if (!_current.HasValue)
				throw new LexingException("It is illegal to access the Current token at this time");

			return _current.Value;
		}
	}

	public void Dispose()
	{
		_reader.Dispose();
	}

	object System.Collections.IEnumerator.Current
	{
		get { return Current; }
	}

	/// <summary>
	/// Consumes input and identifies the next token. 
	/// If no input is available returns <code>false</code>, 
	/// otherwise returns true. 
	/// The next token is then available through the 
	/// <code>Current</code> property
	/// </summary>
	public bool MoveNext()
	{
		//Skip tokens while GetToken() returns Token.Unknown
		//Return false if GetToken() returns Token.EOF,
		//otherwise return true
		if (_current.HasValue && _current.Value == Token.EOF)
			return false;

		do
		{
			_current = GetToken();
		}
		while (_current == Token.UNKNOWN);

		return true;
	}

	public void Reset()
	{
		throw new NotImplementedException();
	}
}
}
