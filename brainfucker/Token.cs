﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace brainfucker
{
	public enum Token
	{
		GT,
		LT,
		PLUS,
		MINUS,
		PERIOD,
		COMMA,
		LBRACKET,
		RBRACKET,
		EOF,
		UNKNOWN
	}
}
