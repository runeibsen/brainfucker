﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using brainfucker.Ast;

namespace brainfucker
{
	public class Parser
	{
		public Ast.Program Parse(IEnumerator<Token> tokenStream)
		{
			List<Operation> operations = new List<Operation>();
			while(tokenStream.MoveNext())
			{
				if (tokenStream.Current == Token.EOF)
					break;

				Operation op = ConsumeOperation(tokenStream);
				operations.Add(op);
			}

			return new Ast.Program(operations);
		}

		private Operation ParseLoop(IEnumerator<Token> tokenStream)
		{
			List<Operation> operations = new List<Operation>();
			
			while (tokenStream.MoveNext() && tokenStream.Current != Token.RBRACKET)
			{
				Operation op = ConsumeOperation(tokenStream);
				operations.Add(op);
			}

			if (tokenStream.Current == Token.RBRACKET)
			{
				return new LoopOperation(operations);
			}

			throw new ParseException("Unexpected end of input when parsing loop");
		}

		private Operation ConsumeOperation(IEnumerator<Token> tokenStream)
		{
			Operation op;
			switch (tokenStream.Current)
			{
				case Token.COMMA:
					op = new ReadData(); break;
				case Token.PERIOD:
					op = new WriteData(); break;
				case Token.GT:
					op = new IncrementDataPointer(); break;
				case Token.LT:
					op = new DecrementDataPointer(); break;
				case Token.PLUS:
					op = new IncrementData(); break;
				case Token.MINUS:
					op = new DecrementData(); break;
				case Token.LBRACKET:
					op = ParseLoop(tokenStream); break;
				default:
					throw new ParseException(String.Format("Syntax error: unexpected token '{0}'", tokenStream.Current));
			}
			return op;
		}
	}
}
