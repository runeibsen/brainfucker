﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection.Emit;

namespace brainfucker
{
	class Program
	{
		static void Main(string[] args)
		{
			if (args.Length != 1 || !(args[0].StartsWith("/f:") || args[0].StartsWith("/t:")))
			{
				Console.WriteLine("Usage: brainfucker /f:<filename>");
				Console.WriteLine("Usage: brainfucker /t:<programtext>");
				return;
			}

			string modeFlag = args[0].Substring(0, 2);
			string inputParameterValue = args[0].Substring(3);
			InputMode inputMode = GetInputMode(modeFlag);
			TextReader reader = GetProgramText(inputMode, inputParameterValue);

			Lexer lexer = new Lexer(reader);
			Parser parser = new Parser();
			Ast.Program program = parser.Parse(lexer);
			CodeGenerator generator = new CodeGenerator();
			AssemblyBuilder asm = generator.Generate(program, "bfmodule.exe");

			asm.Save("bfmodule.exe");
		}

		private static TextReader GetProgramText(InputMode inputMode, string inputParameterValue)
		{
			switch(inputMode)
			{
				case InputMode.File:
					return File.OpenText(inputParameterValue);
				case InputMode.Text:
					return new StringReader(inputParameterValue);
				default:
					throw new ArgumentException(String.Format("Unrecognized input mode: {0}", inputMode));
			}
		}

		private static InputMode GetInputMode(string modeFlag)
		{
			InputMode inputMode;
			switch (modeFlag)
			{
				case "/f":
					inputMode = InputMode.File; break;
				case "/t":
					inputMode = InputMode.Text; break;
				default:
					throw new ArgumentException(String.Format("Input mode not recognized: {0}", modeFlag));
			}
			return inputMode;
		}

		private enum InputMode { File, Text }
	}

	
}
