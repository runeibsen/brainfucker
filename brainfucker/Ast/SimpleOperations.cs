﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace brainfucker.Ast
{
	public class IncrementDataPointer : Operation
	{
		public override void Accept(IVisitor visitor)
		{
			visitor.Visit(this);
		}
	}

	public class DecrementDataPointer : Operation
	{
		public override void Accept(IVisitor visitor)
		{
			visitor.Visit(this);
		}
	}

	public class IncrementData : Operation
	{
		public override void Accept(IVisitor visitor)
		{
			visitor.Visit(this);
		}
	}

	public class DecrementData : Operation
	{
		public override void Accept(IVisitor visitor)
		{
			visitor.Visit(this);
		}
	}

	public class WriteData : Operation
	{
		public override void Accept(IVisitor visitor)
		{
			visitor.Visit(this);
		}
	}

	public class ReadData : Operation
	{
		public override void Accept(IVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}
