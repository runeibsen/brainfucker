﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace brainfucker.Ast
{
	public abstract class Operation
	{
		public abstract void Accept(IVisitor visitor);
		public virtual IEnumerable<Operation> SubOperations { get { return Enumerable.Empty<Operation>(); } }
	}
}
