﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace brainfucker.Ast
{
	public class LoopOperation : Operation
	{
		private readonly IEnumerable<Operation> _operations;

		public LoopOperation(IEnumerable<Operation> operations)
		{
			_operations = operations;
		}

		public override IEnumerable<Operation> SubOperations
		{
			get
			{
				return _operations;
			}
		}

		public override void Accept(IVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}
