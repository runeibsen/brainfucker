﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace brainfucker.Ast
{
	public interface IVisitor
	{
		void Visit(Program operation);
		void Visit(IncrementDataPointer operation);
		void Visit(DecrementDataPointer operation);
		void Visit(IncrementData operation);
		void Visit(DecrementData operation);
		void Visit(WriteData operation);
		void Visit(ReadData operation);
		void Visit(LoopOperation operation);
	}
}
